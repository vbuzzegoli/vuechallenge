/* ============
 * State of the auth module
 * ============
 *
 * The initial state of the auth module.
 */

export default {
	currentIndex: 0,
	artists: []
};
