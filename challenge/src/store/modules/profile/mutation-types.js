/* ============
 * Mutation types for the account module
 * ============
 *
 * The mutation types that are available
 * on the auth module.
 */

export const INC = "INC";
export const DEC = "DEC";
export const FETCHED = "FETCHED";
export const CLEAR = "CLEAR";

export default {
	INC,
	DEC,
	FETCHED,
	CLEAR
};
