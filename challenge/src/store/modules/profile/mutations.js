/* ============
 * Mutations for the auth module
 * ============
 *
 * The mutations that are available on the
 * account module.
 */

import Vue from "vue";
import { INC, DEC, FETCHED, CLEAR } from "./mutation-types";

/* eslint-disable no-param-reassign */
export default {
  [INC](state) {
    state.currentIndex += 1;
  },

  [DEC](state) {
    state.currentIndex -= 1;
  },

  [FETCHED](state, payload) {
    state.artists = payload;
    console.log("Artists were updated: ", state.artists);
  },

  [CLEAR](state) {
    state.artists = [];
    console.log("Results cleared.");
  }
};

